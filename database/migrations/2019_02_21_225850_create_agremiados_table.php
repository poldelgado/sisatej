<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgremiadosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agremiados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('apellido_y_nombre');
            $table->integer('legajo')->unique();
            $table->integer('dni')->unique();
            $table->date('fecha_ingreso')->nullable();
            $table->string('email');
            $table->integer('hijos_menores');
            $table->string('celular');
            $table->integer('cargo_id')->unsigned();
            $table->foreign('cargo_id')->references('id')->on('cargos');
            $table->float('aporte')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agremiados');
    }
}
