<!-- Mes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mes', 'Mes:') !!}
    {!! Form::text('mes', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_total', 'Monto Total:') !!}
    {!! Form::text('monto_total', null, ['class' => 'form-control']) !!}
</div>

<!-- Anio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anio', 'Anio:') !!}
    {!! Form::text('anio', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('aportes.index') !!}" class="btn btn-default">Cancel</a>
</div>
