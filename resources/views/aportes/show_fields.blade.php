<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $aporte->id !!}</p>
</div>

<!-- Mes Field -->
<div class="form-group">
    {!! Form::label('mes', 'Mes:') !!}
    <p>{!! $aporte->mes !!}</p>
</div>

<!-- Monto Total Field -->
<div class="form-group">
    {!! Form::label('monto_total', 'Monto Total:') !!}
    <p>{!! $aporte->monto_total !!}</p>
</div>

<!-- Anio Field -->
<div class="form-group">
    {!! Form::label('anio', 'Anio:') !!}
    <p>{!! $aporte->anio !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $aporte->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $aporte->updated_at !!}</p>
</div>

