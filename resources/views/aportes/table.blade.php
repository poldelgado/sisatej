<table class="table table-responsive" id="aportes-table">
    <thead>
        <tr>
            <th>Mes</th>
        <th>Monto Total</th>
        <th>Anio</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($aportes as $aporte)
        <tr>
            <td>{!! $aporte->mes !!}</td>
            <td>{!! $aporte->monto_total !!}</td>
            <td>{!! $aporte->anio !!}</td>
            <td>
                {!! Form::open(['route' => ['aportes.destroy', $aporte->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('aportes.show', [$aporte->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('aportes.edit', [$aporte->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>