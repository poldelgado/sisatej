@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Aporte
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($aporte, ['route' => ['aportes.update', $aporte->id], 'method' => 'patch']) !!}

                        @include('aportes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection