<!-- Apellido Y Nombre Field -->
<div class="form-group">
    {!! Form::label('apellido_y_nombre', 'Apellido Y Nombre:') !!}
    <p>{!! $agremiado->apellido_y_nombre !!}</p>
</div>

<!-- Legajo Field -->
<div class="form-group">
    {!! Form::label('legajo', 'Legajo:') !!}
    <p>{!! $agremiado->legajo !!}</p>
</div>

<!-- Dni Field -->
<div class="form-group">
    {!! Form::label('dni', 'Dni:') !!}
    <p>{!! $agremiado->dni !!}</p>
</div>

<!-- Fecha Ingreso Field -->
<div class="form-group">
    {!! Form::label('fecha_ingreso', 'Fecha Ingreso:') !!}
    <p>{!! $agremiado->fecha_ingreso !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $agremiado->email !!}</p>
</div>

<!-- Hijos Menores Field -->
<div class="form-group">
    {!! Form::label('hijos_menores', 'Hijos Menores:') !!}
    <p>{!! $agremiado->hijos_menores !!}</p>
</div>

<!-- Celular Field -->
<div class="form-group">
    {!! Form::label('celular', 'Celular:') !!}
    <p>{!! $agremiado->celular !!}</p>
</div>

