@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Agremiado
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            Hola Mundo estos son los cargos:
            {{ $cargos  }}
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'agremiados.store']) !!}

                        @include('agremiados.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
