@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Agremiado
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($agremiado, ['route' => ['agremiados.update', $agremiado->id], 'method' => 'patch']) !!}

                        @include('agremiados.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection