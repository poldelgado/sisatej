<table class="table table-responsive" id="agremiados-table">
    <thead>
        <tr>
            <th>Apellido Y Nombre</th>
        <th>Legajo</th>
        <th>Dni</th>
        <th>Fecha Ingreso</th>
        <th>Email</th>
        <th>Hijos Menores</th>
        <th>Celular</th>
        <th>Cargo Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($agremiados as $agremiado)
        <tr>
            <td>{!! $agremiado->apellido_y_nombre !!}</td>
            <td>{!! $agremiado->legajo !!}</td>
            <td>{!! $agremiado->dni !!}</td>
            <td>{!! $agremiado->fecha_ingreso !!}</td>
            <td>{!! $agremiado->email !!}</td>
            <td>{!! $agremiado->hijos_menores !!}</td>
            <td>{!! $agremiado->celular !!}</td>
            <td>{!! $agremiado->cargo_id !!}</td>
            <td>
                {!! Form::open(['route' => ['agremiados.destroy', $agremiado->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('agremiados.show', [$agremiado->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('agremiados.edit', [$agremiado->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>