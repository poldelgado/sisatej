<!-- Apellido Y Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apellido_y_nombre', 'Apellido Y Nombre:') !!}
    {!! Form::text('apellido_y_nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Legajo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('legajo', 'Legajo:') !!}
    {!! Form::text('legajo', null, ['class' => 'form-control']) !!}
</div>

<!-- Dni Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dni', 'Dni:') !!}
    {!! Form::text('dni', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Ingreso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_ingreso', 'Fecha Ingreso:') !!}
    {!! Form::date('fecha_ingreso', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Hijos Menores Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hijos_menores', 'Hijos Menores:') !!}
    {!! Form::text('hijos_menores', null, ['class' => 'form-control']) !!}
</div>

<!-- Celular Field -->
<div class="form-group col-sm-6">
    {!! Form::label('celular', 'Celular:') !!}
    {!! Form::text('celular', null, ['class' => 'form-control']) !!}
</div>

<!-- Cargo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cargo_id', 'Cargo') !!}
    <select class="form-control" name="cargo_id">
        @foreach($cargos as $cargo)
            <option value="{{$cargo->id}}">{{$cargo->nombre_cargo}}</option>
        @endforeach
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('agremiados.index') !!}" class="btn btn-default">Cancel</a>
</div>
