<li class="{{ Request::is('personas*') ? 'active' : '' }}">
    <a href="{!! route('personas.index') !!}"><i class="fa fa-edit"></i><span>Personas</span></a>
</li>

<li class="{{ Request::is('agremiados*') ? 'active' : '' }}">
    <a href="{!! route('agremiados.index') !!}"><i class="fa fa-edit"></i><span>Agremiados</span></a>
</li>

<li class="{{ Request::is('cargos*') ? 'active' : '' }}">
    <a href="{!! route('cargos.index') !!}"><i class="fa fa-edit"></i><span>Cargos</span></a>
</li>

<li class="{{ Request::is('aportes*') ? 'active' : '' }}">
    <a href="{!! route('aportes.index') !!}"><i class="fa fa-edit"></i><span>Aportes</span></a>
</li>

