<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cargo->id !!}</p>
</div>

<!-- Nombre Cargo Field -->
<div class="form-group">
    {!! Form::label('nombre_cargo', 'Nombre Cargo:') !!}
    <p>{!! $cargo->nombre_cargo !!}</p>
</div>

<!-- Categoria Field -->
<div class="form-group">
    {!! Form::label('categoria', 'Categoria:') !!}
    <p>{!! $cargo->categoria !!}</p>
</div>

<!-- Sueldo Basico Field -->
<div class="form-group">
    {!! Form::label('sueldo_basico', 'Sueldo Basico:') !!}
    <p>{!! $cargo->sueldo_basico !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cargo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cargo->updated_at !!}</p>
</div>

