<!-- Nombre Cargo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_cargo', 'Nombre Cargo:') !!}
    {!! Form::text('nombre_cargo', null, ['class' => 'form-control']) !!}
</div>

<!-- Categoria Field -->
<div class="form-group col-sm-6">
    {!! Form::label('categoria', 'Categoria:') !!}
    {!! Form::text('categoria', null, ['class' => 'form-control']) !!}
</div>

<!-- Sueldo Basico Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sueldo_basico', 'Sueldo Basico:') !!}
    {!! Form::text('sueldo_basico', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cargos.index') !!}" class="btn btn-default">Cancel</a>
</div>
