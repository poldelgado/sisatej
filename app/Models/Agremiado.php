<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Agremiado
 * @package App\Models
 * @version February 21, 2019, 10:58 pm UTC
 *
 * @property string apellido_y_nombre
 * @property integer legajo
 * @property integer dni
 * @property date fecha_ingreso
 * @property string email
 * @property integer hijos_menores
 * @property string celular
 * @property integer cargo_id
 */
class Agremiado extends Model
{
    use SoftDeletes;

    public $table = 'agremiados';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'apellido_y_nombre',
        'legajo',
        'dni',
        'fecha_ingreso',
        'email',
        'hijos_menores',
        'celular',
        'cargo_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'apellido_y_nombre' => 'string',
        'legajo' => 'integer',
        'dni' => 'integer',
        'fecha_ingreso' => 'date',
        'email' => 'string',
        'hijos_menores' => 'integer',
        'celular' => 'string',
        'cargo_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

     public function cargos()
    {
        return $this->hasOne('App\Models\Cargo');
    }

    
}
