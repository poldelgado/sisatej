<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cargo
 * @package App\Models
 * @version February 21, 2019, 11:05 pm UTC
 *
 * @property string nombre_cargo
 * @property string categoria
 * @property float sueldo_basico
 */
class Cargo extends Model
{
    use SoftDeletes;

    public $table = 'cargos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre_cargo',
        'categoria',
        'sueldo_basico'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre_cargo' => 'string',
        'categoria' => 'string',
        'sueldo_basico' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

   public function Agremiado()
{
    return $this->belongsTo('App\Model\Agremiado');
}

    
}
