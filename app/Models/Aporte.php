<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Aporte
 * @package App\Models
 * @version February 21, 2019, 11:42 pm UTC
 *
 * @property integer mes
 * @property float monto_total
 * @property integer anio
 */
class Aporte extends Model
{
    use SoftDeletes;

    public $table = 'aportes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'mes',
        'monto_total',
        'anio'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'mes' => 'integer',
        'monto_total' => 'float',
        'anio' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
