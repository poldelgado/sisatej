<?php

namespace App\Http\Controllers;


use App\Http\Requests\CreateAgremiadoRequest;
use App\Http\Requests\UpdateAgremiadoRequest;
use App\Models\Agremiado;
use App\Repositories\AgremiadoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Cargo;

class AgremiadoController extends AppBaseController
{
    /** @var  AgremiadoRepository */
    private $agremiadoRepository;

    public function __construct(AgremiadoRepository $agremiadoRepo)
    {
        $this->agremiadoRepository = $agremiadoRepo;
    }

    /**
     * Display a listing of the Agremiado.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->agremiadoRepository->pushCriteria(new RequestCriteria($request));
        $agremiados = $this->agremiadoRepository->all();

        return view('agremiados.index')
            ->with('agremiados', $agremiados);
    }

    /**
     * Show the form for creating a new Agremiado.
     *
     * @return Response
     */
    public function create()
    {
        $cargos = Cargo::orderBy('nombre_cargo')->get();
        return view('agremiados.create')->with('cargos', $cargos);
    }

    /**
     * Store a newly created Agremiado in storage.
     *
     * @param CreateAgremiadoRequest $request
     *
     * @return Response
     */
    public function store(CreateAgremiadoRequest $request)
    {
        $this->validate($request, array(
            'apellido_y_nombre' => 'required|max:100',
            'legajo' => 'required|unique:agremiados',
            'dni' => 'required|unique:agremiados',
            'email' => 'required|email',
            'hijos_menores' => 'required|integer',
            'fecha_ingreso' => 'required|date',
            'celular' => 'required',
            'cargo_id' => 'required',
        ));

        $agremiado = new Agremiado();
        $agremiado->apellido_y_nombre = $request->apellido_y_nombre;
        $agremiado->legajo = $request->legajo;
        $agremiado->dni = $request->dni;
        $agremiado->email = $request->email;
        $agremiado->hijos_menores = $request->hijos_menores;
        $agremiado->fecha_ingreso = $request->fecha_ingreso;
        $agremiado->celular = $request->celular;
        //busco el cargo elegido
        $cargo = Cargo::find($request->cargo_id);
        $agremiado->cargo_id = $request->cargo_id;
        $agremiado->aporte = $cargo->sueldo_basico * 0.02;

        if($agremiado->save())
            Flash::success('Agremiado guardado correctamente.');
        else
            Flash::success('Ocurrio un error al guardar el agremiado.');

        return redirect(route('agremiados.index'));
    }

    /**
     * Display the specified Agremiado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agremiado = $this->agremiadoRepository->findWithoutFail($id);

        if (empty($agremiado)) {
            Flash::error('Agremiado not found');

            return redirect(route('agremiados.index'));
        }

        return view('agremiados.show')->with('agremiado', $agremiado);
    }

    /**
     * Show the form for editing the specified Agremiado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agremiado = $this->agremiadoRepository->findWithoutFail($id);

        if (empty($agremiado)) {
            Flash::error('Agremiado not found');

            return redirect(route('agremiados.index'));
        }

        return view('agremiados.edit')->with('agremiado', $agremiado);
    }

    /**
     * Update the specified Agremiado in storage.
     *
     * @param  int              $id
     * @param UpdateAgremiadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgremiadoRequest $request)
    {
        $agremiado = $this->agremiadoRepository->findWithoutFail($id);

        if (empty($agremiado)) {
            Flash::error('Agremiado not found');

            return redirect(route('agremiados.index'));
        }

        $agremiado = $this->agremiadoRepository->update($request->all(), $id);

        Flash::success('Agremiado updated successfully.');

        return redirect(route('agremiados.index'));
    }

    /**
     * Remove the specified Agremiado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $agremiado = $this->agremiadoRepository->findWithoutFail($id);

        if (empty($agremiado)) {
            Flash::error('Agremiado not found');

            return redirect(route('agremiados.index'));
        }

        $this->agremiadoRepository->delete($id);

        Flash::success('Agremiado deleted successfully.');

        return redirect(route('agremiados.index'));
    }
}
