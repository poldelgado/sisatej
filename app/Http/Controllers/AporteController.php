<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAporteRequest;
use App\Http\Requests\UpdateAporteRequest;
use App\Repositories\AporteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AporteController extends AppBaseController
{
    /** @var  AporteRepository */
    private $aporteRepository;

    public function __construct(AporteRepository $aporteRepo)
    {
        $this->aporteRepository = $aporteRepo;
    }

    /**
     * Display a listing of the Aporte.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->aporteRepository->pushCriteria(new RequestCriteria($request));
        $aportes = $this->aporteRepository->all();

        return view('aportes.index')
            ->with('aportes', $aportes);
    }

    /**
     * Show the form for creating a new Aporte.
     *
     * @return Response
     */
    public function create()
    {
        return view('aportes.create');
    }

    /**
     * Store a newly created Aporte in storage.
     *
     * @param CreateAporteRequest $request
     *
     * @return Response
     */
    public function store(CreateAporteRequest $request)
    {
        $input = $request->all();

        $aporte = $this->aporteRepository->create($input);

        Flash::success('Aporte saved successfully.');

        return redirect(route('aportes.index'));
    }

    /**
     * Display the specified Aporte.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $aporte = $this->aporteRepository->findWithoutFail($id);

        if (empty($aporte)) {
            Flash::error('Aporte not found');

            return redirect(route('aportes.index'));
        }

        return view('aportes.show')->with('aporte', $aporte);
    }

    /**
     * Show the form for editing the specified Aporte.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $aporte = $this->aporteRepository->findWithoutFail($id);

        if (empty($aporte)) {
            Flash::error('Aporte not found');

            return redirect(route('aportes.index'));
        }

        return view('aportes.edit')->with('aporte', $aporte);
    }

    /**
     * Update the specified Aporte in storage.
     *
     * @param  int              $id
     * @param UpdateAporteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAporteRequest $request)
    {
        $aporte = $this->aporteRepository->findWithoutFail($id);

        if (empty($aporte)) {
            Flash::error('Aporte not found');

            return redirect(route('aportes.index'));
        }

        $aporte = $this->aporteRepository->update($request->all(), $id);

        Flash::success('Aporte updated successfully.');

        return redirect(route('aportes.index'));
    }

    /**
     * Remove the specified Aporte from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $aporte = $this->aporteRepository->findWithoutFail($id);

        if (empty($aporte)) {
            Flash::error('Aporte not found');

            return redirect(route('aportes.index'));
        }

        $this->aporteRepository->delete($id);

        Flash::success('Aporte deleted successfully.');

        return redirect(route('aportes.index'));
    }
}
