<?php

namespace App\Repositories;

use App\Models\Agremiado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgremiadoRepository
 * @package App\Repositories
 * @version February 21, 2019, 10:58 pm UTC
 *
 * @method Agremiado findWithoutFail($id, $columns = ['*'])
 * @method Agremiado find($id, $columns = ['*'])
 * @method Agremiado first($columns = ['*'])
*/
class AgremiadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'apellido_y_nombre',
        'legajo',
        'dni',
        'fecha_ingreso',
        'email',
        'hijos_menores',
        'celular',
        'cargo_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agremiado::class;
    }
}
