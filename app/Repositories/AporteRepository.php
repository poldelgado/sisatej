<?php

namespace App\Repositories;

use App\Models\Aporte;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AporteRepository
 * @package App\Repositories
 * @version February 21, 2019, 11:42 pm UTC
 *
 * @method Aporte findWithoutFail($id, $columns = ['*'])
 * @method Aporte find($id, $columns = ['*'])
 * @method Aporte first($columns = ['*'])
*/
class AporteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mes',
        'monto_total',
        'anio'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Aporte::class;
    }
}
